package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;

import javax.swing.plaf.nimbus.State;


public class DBManager {

	private static DBManager instance;

	private static final String APP_PROPERTIES_FILE = "app.properties";
	private static final String PROPERTIES_URL = "connection.url";

	public static synchronized DBManager getInstance() {
		if (instance == null)
			instance = new DBManager();

		return instance;
	}

	private DBManager() {
	}


	static final class USER {
		// fields
		public static final String ID = "id";
		public static final String LOGIN = "login";

		// queries
		public static final String GET_ALL_USERS = "SELECT * FROM users";
		public static final String FIND_USERS_BY_LOGIN = "SELECT * FROM users WHERE login = ?";
		public static final String INSERT_USER = "INSERT INTO users VALUES (DEFAULT, ?)";
		public static final String DELETE_USERS_BY_LOGIN = "DELETE FROM users " +
				"WHERE login = ?";

	}

	static final class TEAM {
		// fields
		public static final String ID = "id";
		public static final String NAME = "name";

		// queries
		public static final String GET_ALL_TEAMS = "SELECT * FROM teams";
		public static final String FIND_TEAMS_BY_NAME = "SELECT * FROM teams WHERE name = ?";
		public static final String INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT, ?)";
		public static final String DELETE_TEAMS_BY_NAME = "DELETE FROM teams " +
				"WHERE name = ?";



	}

	public List<User> findAllUsers() throws DBException {
		List<User> list = new ArrayList<>();

		Connection connection = null;
		try {
			Properties properties = loadPropertiesFile();
			connection = DriverManager.getConnection(properties.getProperty(PROPERTIES_URL));

			String query = USER.GET_ALL_USERS;
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(query);

			while (resultSet.next()) {
				User user = User.createUser(
						resultSet.getString(USER.LOGIN)
				);
				list.add(user);
			}

		} catch (IOException | SQLException e) {
			e.printStackTrace();
			throw new DBException("Problem occurred while selecting all users", e.getCause());
		} finally {
			if (connection != null)
				closeConnection(connection);
		}

		return list;
	}

	public boolean insertUser(User user) throws DBException {

		Connection connection = null;

		try {
			Properties properties = loadPropertiesFile();
			connection = DriverManager.getConnection(properties.getProperty(PROPERTIES_URL));

			try (PreparedStatement statement = connection.prepareStatement(
					USER.INSERT_USER,
					Statement.RETURN_GENERATED_KEYS
			)) {
				statement.setString(1, user.getLogin());
				int affectedRecords = statement.executeUpdate();

				if (affectedRecords == 0)
					throw new DBException("Problem occurred while inserting user", new SQLException());
				try (ResultSet generatedKey = statement.getGeneratedKeys()) {
					if (generatedKey.next()) {
						int index = generatedKey.getInt(1);
						user.setId(index);
					}
				}
			}
		} catch (IOException | SQLException e) {
			e.printStackTrace();
			throw new DBException("Problem occurred while inserting user", e.getCause());
		} finally {
			closeConnection(connection);
		}

		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {

		try (Connection connection = DriverManager.getConnection(loadPropertiesFile().getProperty(PROPERTIES_URL))) {
			try (PreparedStatement statement = connection.prepareStatement(USER.DELETE_USERS_BY_LOGIN)) {
				for (User user: users) {
					statement.setString(1, user.getLogin());
					statement.executeUpdate();
				}
			}

		} catch (IOException | SQLException e) {
			e.printStackTrace();
			throw new DBException("Problem occurred while deleting user", e.getCause());
		}

		return true;
	}

	public User getUser(String login) throws DBException {

		User user = null;
		Connection connection = null;
		try {
			Properties properties = loadPropertiesFile();
			connection = DriverManager.getConnection(properties.getProperty(PROPERTIES_URL));

			PreparedStatement statement = connection.prepareStatement(USER.FIND_USERS_BY_LOGIN);
			statement.setString(1, login);

			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				String log = resultSet.getString(USER.LOGIN);
				user = User.createUser(log);
			}

		} catch (IOException | SQLException e) {
			e.printStackTrace();
			throw new DBException("Problem occurred while finding user by login", e.getCause());
		} finally {
			if (connection != null)
				closeConnection(connection);
		}

		return user;
	}

	public Team getTeam(String name) throws DBException {

		Team team = null;
		Connection connection = null;
		try {
			Properties props = loadPropertiesFile();
			connection = DriverManager.getConnection(props.getProperty(PROPERTIES_URL));

			PreparedStatement statement = connection.prepareStatement(TEAM.FIND_TEAMS_BY_NAME);
			statement.setString(1, name);
			ResultSet resultSet = statement.executeQuery();

			if (resultSet.next())
				team = Team.createTeam(resultSet.getString(TEAM.NAME));

		} catch (IOException | SQLException e) {
			e.printStackTrace();
			throw new DBException("Problem occurred while finding team", e.getCause());
		} finally {
			closeConnection(connection);
		}

		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> list = new ArrayList<>();

		Connection connection = null;
		try {
			Properties properties = loadPropertiesFile();
			connection = DriverManager.getConnection(properties.getProperty(PROPERTIES_URL));

			String query = TEAM.GET_ALL_TEAMS;
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(query);

			while (resultSet.next()) {
				Team team = Team.createTeam(
						resultSet.getString(TEAM.NAME)
				);
				list.add(team);
			}

		} catch (IOException | SQLException e) {
			e.printStackTrace();
			throw new DBException("Problem occurred while selecting all teams", e.getCause());
		} finally {
			if (connection != null)
				closeConnection(connection);
		}

		return list;
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection connection = null;

		try {
			Properties properties = loadPropertiesFile();
			connection = DriverManager.getConnection(properties.getProperty(PROPERTIES_URL));

			try (PreparedStatement statement = connection.prepareStatement(
					TEAM.INSERT_TEAM,
					Statement.RETURN_GENERATED_KEYS
			)) {
				statement.setString(1, team.getName());
				int affectedRecords = statement.executeUpdate();

				if (affectedRecords == 0)
					throw new DBException("Problem occurred while inserting team", new SQLException());
				try (ResultSet generatedKey = statement.getGeneratedKeys()) {
					if (generatedKey.next()) {
						int index = generatedKey.getInt(1);
						team.setId(index);
					}
				}
			}
		} catch (IOException | SQLException e) {
			e.printStackTrace();
			throw new DBException("Problem occurred while inserting team", e.getCause());
		} finally {
			closeConnection(connection);
		}

		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {

		Connection connection = null;

		try {
			connection = DriverManager.getConnection(loadPropertiesFile().getProperty(PROPERTIES_URL));

			try (PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO users_teams VALUES (?, ?)"
			)) {

				connection.setAutoCommit(false);

				if (user.getId() == 0) {
					statement.setNull(1, Types.INTEGER);
				} else {
					statement.setInt(1, user.getId());
				}
				for (Team team: teams) {
					if (team.getId() == 0) {
						statement.setNull(2, Types.INTEGER);
					} else {
						statement.setInt(2, team.getId());
					}
					statement.executeUpdate();
				}

				connection.commit();
				connection.setAutoCommit(true);
			}

		} catch (SQLException | IOException e) {
			try {
				connection.rollback();
				connection.setAutoCommit(true);
			} catch (SQLException ex) {
				ex.printStackTrace();
				throw new DBException("Problem occurred while roolbacking transaction", ex.getCause());
			}
			e.printStackTrace();
			throw new DBException("Problem occurred while setting teams for user", e.getCause());
		} finally {
			closeConnection(connection);
		}

		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();

		try (Connection connection = DriverManager.getConnection(loadPropertiesFile().getProperty(PROPERTIES_URL))) {

			String query = "SELECT * FROM teams " +
					"INNER JOIN users_teams " +
					"ON id = team_id " +
					"WHERE user_id = ?";

			try (PreparedStatement statement = connection.prepareStatement(query)) {
				if (user.getId() == 0) {
					statement.setNull(1, Types.INTEGER);
				} else {
					statement.setInt(1, user.getId());
				}
				ResultSet resultSet = statement.executeQuery();

				while (resultSet.next()) {
					teams.add(
							Team.createTeam(resultSet.getString(TEAM.NAME))
					);
				}
			}

		} catch (IOException | SQLException e) {
			e.printStackTrace();
			throw new DBException("Problem occurred while selecting teams from user teams", e.getCause());

		}

		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try (Connection connection = DriverManager.getConnection(loadPropertiesFile().getProperty(PROPERTIES_URL))) {
			try (PreparedStatement statement = connection.prepareStatement(TEAM.DELETE_TEAMS_BY_NAME)) {
				statement.setString(1, team.getName());
				statement.executeUpdate();
			}

		} catch (IOException | SQLException e) {
			e.printStackTrace();
			throw new DBException("Problem occurred while deleting user", e.getCause());
		}

		return true;
	}

	public boolean updateTeam(Team team) throws DBException {

		try (Connection connection = DriverManager.getConnection(loadPropertiesFile().getProperty(PROPERTIES_URL))) {

			String query = "UPDATE teams " +
					"SET name = ? " +
					"WHERE id = ?";
			try (PreparedStatement statement = connection.prepareStatement(query)) {
				statement.setString(1, team.getName());
				statement.setInt(2, team.getId());
				statement.executeUpdate();
			}
		} catch (IOException | SQLException e) {
			e.printStackTrace();
			throw new DBException("failed message", e.getCause());
		}

		return true;
	}

	private static Properties loadPropertiesFile() throws IOException {
		Properties properties = new Properties();
		properties.load(new FileInputStream(APP_PROPERTIES_FILE));

		return properties;
	}

	private static void closeConnection(AutoCloseable closeable) throws DBException {
		if (closeable == null)
			return;

		try {
			closeable.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException("Problem occurred while closing connection", e.getCause());
		}
	}

}
